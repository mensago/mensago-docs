= Mensago Contact Information and Address Books
:author: Jon Yoder
:email: jon@yoder.cloud
:revdate: 2025-01-18
:revnumber: 1.4
:description: Structure and description of Mensago contact information and processes
:keywords: Mensago, contacts
:toc: preamble
:table-stripes: odd

*Status:* Draft (incomplete) +
*Abstract:* Mensago contact information and processes

== Description

Contact information and address book synchronization are one reason Mensago is a platform and not merely a new email application. This document describes how Mensago contact information is structured, stored, communicated, and shared.

== Structure

Like other Mensago data, contact information is exchanged in JSON format. At the top level, a contact is just a dictionary of fields. A short example of the format is shown below.

[source,json]
----
{
    "Type" : "application/vnd.mensago.contact",
    "Version" : "1.0.0",
    "ID" : "75961a13-2d56-4702-a5f6-efd51f40a087",
    "EntityType" : "individual",
    "SharingLevel" : "Full Private",

    "GivenName" : "Fred",
    "FamilyName" : "Kingsley",
    "Gender" : "male",
    "Birthday" : "1986-03-14",
    "UserID" : "fkingsley",
    "Workspace" : "3f258107-19aa-43c1-86c6-30e3914e18b8",
    "Domain" : "example.com"
}
----

*Example:*

Below is listed an example of a profile which utilizes most of the available fields so that the structure of a contact can be better understood. 

[source,json]
----
{
    "Type" : "application/vnd.mensago.contact",
    "Version" : "1.0.0",
    "ID" : "75961a13-2d56-4702-a5f6-efd51f40a087",
    "EntityType" : "individual",
    "SharingLevel" : "Full Private",

    "FormattedName" : "Richard Brannan",
    "GivenName" : "Richard",
    "FamilyName" : "Brannan",
    "MiddleName" : "Michael",
    "Prefix" : "Mr.",
    "Suffix" : "Ph.D",
    
    "Gender" : "Male",
    "Bio" : "The misadventures of some guy",
    "Social" : [
        {   "Label" : "Twitter",
            "Value" : "@richardbrannan19034"
        }
    ],

    "UserID" : "cavs4life",
    "Workspace" : "e3e98b45-b226-4926-b4ea-69ab16dd035e",
    "Domain" : "example.com",

    "Chat" : [
        {   "Label" : "Signal",
            "Value" : "555-555-9090"
        }
    ],

    "Keys" : [
        {   "Label" : "Signing",
            "Value" : "ED25519:6|HBWrxMY6-?r&Sm)_^PLPerpqOj#b&x#N_#C3}p"
        },
        {   "Label" : "Encryption",
            "Value" : "CURVE25519:mO?WWA-k2B2O|Z%fA`~s3^$iiN{5R->#jxO@cy6{"
        }
    ],

    "Mail" : [
        {   "Label" : "Work",
            "Street" : "1013 Hickman St.",
            "Extended" : "Suite D",
            "Locality" : "Bensenville",
            "Region" : "Illinois",
            "PostalCode" : "60106",
            "Country" : "United States",
            "Preferred" : false
        },
        {   "Label" : "Home",
            "POBox" : "4315",
            "Locality" : "Bensenville",
            "Region" : "Illinois",
            "PostalCode" : "60106",
            "Country" : "United States",
            "Preferred" : true
        }
    ],

    "Phone" : [
        {   "Label" : "Home",
            "Value" : "555-555-1234"
        },
        {   "Label" : "Work",
            "Value" : "555-555-5678"
        },
        {
            "Label" : "Mobile",
            "Value" : "555-555-9090",
            "Preferred" : true
        }
    ],

    "Anniversary" : "2000-10-04",
    "Birthday" : "1990-04-15",
    "Email" : [
        {   "Label" : "Work",
            "Value" : "rbrannan@contoso.com"
        }
    ],

    "Organization" : "Acme Widgets, Inc.",
    "OrgUnits" : [ "Administration", "Finance" ],
    "Title" : "Chief Financial Officer",

    "Categories" : [ "Executive" ],

    "Websites" : [
        {   "Label": "Personal",
            "Value": "https://www.example.com"
        }
    ],

    "Photo" : {
        "MimeType" : "image/png",
        "Data" : "iBL{Q4GJ0x0000DNk~Le0000A0000A2nGNE0F5%wy#N3J1am@3R0s$N2z&@+hyVZp7)eAyR2Y?G{Qv*|e+D7|6ETWL6;e+j0BM>85Q>cpXaE2J07*qoM6N<$f&"
    },

    "Languages" : [ "en" ],
    
    "Notes" : "Hobbies: chainsaw carving, free climbing, underwater basket weaving"
}
----

== Field Definitions

Each of the fields defined in a contact are described below. Most of these fields map directly to those found in the https://tools.ietf.org/html/rfc6350[vCard standard]. Unlike the vCard standard, almost every top-level field is optional so that a Mensago workspace account can be maintained with the only identifying information for the account being its numeric address. At the same time, even this field is not required so that information for contacts who do not have a Mensago address can be managed. In the interest of simplicity, all fields are strings, lists, or dictionaries.

Type:: REQUIRED. The MIME type of the object.

Version:: REQUIRED. API version of the payload.

ID:: REQUIRED. A RandomID representing the entity in the context of the relationship with another entity. This is to make processing contact information updates much more efficient.

EntityType:: REQUIRED. `EntityType` maps to the vCard field `KIND`. Values are `group`, `individual` (the default), or `org`.

Update:: CONDITIONAL. Describes whether or not the information in the contact is intended to update existing information and contains either the value 'true' or 'false'. This payload field is REQUIRED in contact information update messages, but it is not present in other uses of the contact data.

FormattedName:: OPTIONAL. `Formatted` maps to the vCard field `FN`. This field is the full formatted version of the entity's name, including prefix and suffixes. If omitted, clients should display the value of the `GivenName` field listed below.

GivenName:: REQUIRED. The primary name for an entity. In many cultures, this is an individual's first name. This field is used for the entity's name when the type is `group` or `org`.

FamilyName:: OPTIONAL. The family name for an entity. Used for individuals only.

MiddleName:: OPTIONAL. An additional name for the entity. In English-speaking countries, this is generally an individual's middle name(s) or initial. Used for individuals only.

Prefix:: OPTIONAL. A prefix for an entity. For individuals in the United States, this translates to "Dr", "Mr", "Miss", etc. Used for individuals only.

Suffix:: OPTIONAL. A string containing one or more comma-separated suffixes for an entity, such as "Esq." or "MD". Used for individuals only.

Gender:: OPTIONAL. `Gender` maps to the vCard `GENDER` field's gender identity component, which is a free-form text field. Used for individuals only.

Bio:: OPTIONAL. This is a public-facing biographical string where a user can summarize important aspects of their life and/or personality. For organizations and groups, this field supplies information about the entity similar to an 'About Us' website page.

Social:Label:: CONDITIONAL. Entries in the Social field are optional, but if an entry exists, it MUST have both a Label and a Value. The Label field contains the name of the social network. The label is expected to use the capitalization which exactly matches that used by the network. A not-exhaustive list of examples would include Reddit, Facebook, Instagram, LinkedIn, Diaspora, Mastodon, and Pixelfed. For ease of processing, the label is to be formatted using the official capitalization, spelling, and punctuation used for the network's brand, e.g. "X" for the social network formerly known as Twitter.

Social:Value:: CONDITIONAL. Entries in the Social field are optional, but if an entry exists, it MUST have both a Label and a Value. The Value field contains the URL for the profile page of the entity on the network's website. If the social media network does not have public profile pages for its users, this field contains the identifier for the entity.

Social:Preferred:: OPTIONAL. Contains `yes` or `no` to indicate if it is the preferred social media network. Only one entry can have a `yes` value for this field, but all entries MAY have a `no` value, indicating no preference. If this field is omitted, it is assumed to have a value of `no`. NOTE: this value of this field is ignored if there is only one network defined. 

Phone:Label:: CONDITIONAL. The Phone field is used to store phone numbers for the entity. The Phone field and its entries are optional, but if an entry exists, it MUST have both a Label and a Value. The Label field contains the type of phone, such as 'Home', 'Work, 'Landline', 'Mobile', etc. Note that the vCard field `TEL` roughly maps to this, as the names of the phone numbers are not rigidly defined, unlike the types in the vCard standard. 

Phone:Value:: CONDITIONAL. The Phone field is used to store phone numbers for the entity. The Phone field and its entries are optional, but if an entry exists, it MUST have both a Label and a Value. The Value field contains the entity's phone number. The phone number's format is regional and country codes are optional.

Phone:Preferred:: OPTIONAL. Contains `yes` or `no` to indicate if it is the preferred phone number. Only one entry can have a `yes` value for this field, but all entries MAY have a `no` value, indicating no preference. If this field is omitted, it is assumed to have a value of `no`. NOTE: this value of this field is ignored if there is only one number defined. 

UserID:: OPTIONAL. This field contains the 'friendly' part of the contact's address. If `UserID` is empty or missing, the client MUST use the contact's workspace address, e.g. `cavsfan4life/example.com` or `5ccc9ba6-9d4e-47d0-9c57-11ade969a88b/example.com`.

Workspace:: CONDITIONAL. This field contains the user's workspace ID, i.e. the numeric RandomID identifier used for the entity's account. 

Domain:: CONDITIONAL. `Domain` contains the fully-qualified domain of the contact's address. 

Chat:Label:: CONDITIONAL. The Messaging field is used to store information about instant messaging and chat services utilized by the entity. The Messaging field and its entries are optional, but if an entry exists, it MUST have both a Label and a Value. The Label field contains the name of the messaging service. 

Chat:Value:: CONDITIONAL. The Messaging field is used to store information about instant messaging and chat services utilized by the entity. The Messaging field and its entries are optional, but if an entry exists, it MUST have both a Label and a Value. The Value field contains the identifier for the entity on the messaging service. The format of the identifier depends on the service itself. For example, Signal uses the entity's phone number and Element uses the format `@username@example.com`.

Chat:Preferred:: OPTIONAL. Contains `yes` or `no` to indicate if it is the preferred messaging/chat method. Only one entry can have a `yes` value for this field, but all entries MAY have a `no` value, indicating no preference. If this field is omitted, it is assumed to have a value of `no`. NOTE: this value of this field is ignored if there is only one service defined. 

Keys:: CONDITIONAL. A dictionary containing labels and associated keys. It is required to be present when included in a Contact Requests and MUST contain CryptoString values for `Encryption` and `Verification`.

Mail:: OPTIONAL. This is a list of dictionaries contains mailing address information. The fields used largely map to corresponding parameters of the vCard field `ADR`. The mappings of these fields are explained below in relation to U.S. mailing addresses merely for the sake of clarity. All fields are optional 

Mail:Label:: CONDITIONAL. This field indicates the type of mailing address described, such as 'Home' or 'Work'. It is a required field for each `MailingAddress` entry.

Mail:Street:: OPTIONAL. The street address. Apartment numbers and suite numbers should use `ExtendedAddress`.

Mail:Extended:: OPTIONAL. Apartment or suite numbers should use `ExtendedAddress` and not be included in `StreetAddress`. When in doubt, consult the postal organization for a particular country for how these two fields should be used. 

Mail:Locality:: OPTIONAL. This field maps to the city in a U.S. mailing address. Other locations will probably use this for the same purpose.

Mail:Region:: OPTIONAL. This field maps to the state in a U.S. mailing address. Other locations will probably use this for a similar purpose.

Mail:PostalCode:: OPTIONAL. This field maps to the ZIP code in a U.S. mailing address. Other locations will probably use this for a similar purpose.

Mail:Country:: OPTIONAL. A string describing the country for the address.

Mail:Preferred:: OPTIONAL. Contains `yes` or `no` to indicate if it is the preferred mailing address. Only one entry can have a `yes` value for this field, but all entries MAY have a `no` value, indicating no preference. If this field is omitted, it is assumed to have a value of `no`. NOTE: this value of this field is ignored if there is only one address defined. 

Anniversary:: OPTIONAL. `Anniversary` maps to the vCard field `ANNIVERSARY`. This is the date of marriage or equivalent for the entity. Format is YYYY-MM-DD or MM-DD.

Birthday:: OPTIONAL. `Birthday` maps to the vCard field `BDAY`. The birth date of the entity. Format is YYYY-MM-DD or MM-DD. Organizations and groups can use this field for their date of founding.

Email:: OPTIONAL. This field contains a list of dictionaries which follow the same conventions as `Mail`. Each entry in `Email` maps to an individual vCard `EMAIL` field.

Organization:: OPTIONAL. This field contains the name of the organization to which the entity belongs. It maps to the first parameter of the `ORG` vCard field.

OrgUnits:: OPTIONAL. This field contains a list of departments indicating the hierarchy in which the entity is classified.

Title:: OPTIONAL. `Title` maps to the vCard `TITLE` field. It contains the title or job position of the entity.

Websites:Label:: CONDITIONAL. Entries in the Websites field are optional, but if an entry exists, it MUST have both a Label and a Value. The Label field contains the type of website, such as 'Business', 'Personal', or the actual name of the site.

Websites:Value:: CONDITIONAL. Entries in the Websites field are optional, but if an entry exists, it MUST have both a Label and a Value. The Value field contains the URL of the website.

Websites:Preferred:: OPTIONAL. Contains `yes` or `no` to indicate if it is the preferred website. Only one entry can have a `yes` value for this field, but all entries MAY have a `no` value, indicating no preference. If this field is omitted, it is assumed to have a value of `no`. NOTE: this value of this field is ignored if there is only one address defined. 

Photo:: OPTIONAL. A field group containing photo information for the contact. The `Photo` field is not required, but if present, all of its subfields MUST be present.

Photo:Mime:: CONDITIONAL. This field contains the MIME type of the data stored in the `Data` field. Mensago clients MUST support `image/jxl`, `image/png`, and `image/jpg` display. Because of the general superiority of the format, JPEG XL (JXL) images should be preferred. Support for image types beyond the aforementioned formats is optional. Support for animated profile photos is strongly discouraged.

Photo:Data:: CONDITIONAL. This field contains Base85-encoded file data for the photo. The data in this field MUST be no larger than 500KiB before encoding is applied.

Languages:: OPTIONAL. `Languages` roughly maps to the vCard `LANG` field. It is a list of languages used in communications with the entity. The languages are listed in order of preference from most preferred to least. The codes themselves MUST follow the format established in the https://en.wikipedia.org/wiki/ISO_639-3[ISO 639-3] standard.

Notes:: OPTIONAL. Contains miscellaneous text notes stored in SFTM format or plaintext. This field MUST NOT contain any attachment-type data, such as pictures or other kinds of files, but it MAY contain any other kind of SFTM-permitted data, such as links or tables. Attachment data MUST use the `Attachments` field described below.

NoteFormat:: CONDITIONAL. This field is required if the `Notes` field is used. It MUST have a value of either `text` or `sftm`.

Attachments:: OPTIONAL. This list of field groups contains miscellaneous data intended to be associated with the entity. Although this field is not required, each field group is required to have all fields populated and valid.

Attachments:Name:: CONDITIONAL. It contains the name of the attached data. This name can be a file name, but is not required to be.

Attachments:Mime:: CONDITIONAL. It contains the MIME type of the encoded data.

Attachments:Data:: CONDITIONAL. It contains the actual Base85-encoded data of the attachment.

Custom:: OPTIONAL. The `Custom` field exists to permit storage of non-standard data. It contains a list of dictionary entries, similar to Phone or Messaging. Each entry is expected to have the customary `Label` and `Value` fields. For security reasons, software clients are expected to treat entries in this field as generic text and permit no integration outside of clipboard operations.

== Contact Requests

Unlike email and many other forms of communication, communications with other users on the Mensago platform are on a strictly opt-in basis. A Contact Request exchange similar to those found on social media must take place before any other kind of communication can take place between two entities. The result is a simple, familiar concept which places users in control and provides a means to exchange both encryption keys and contact information. Filtering and organizing communications is part of the design goals of the platform.

The Contact Request process is as follows:

[arabic]
. User #1 receives the approval and is asked if they would like to share any additional personal information with User #2. How much information is shared is up to User #1. This response also includes encryption and signature verification keys which are unique to that contact.

This process makes it possible to exchange information without exposure to infrastructure and with a minimum of back-and-forth. The combination of contact requests and required encryption enables several security advantages:

* Encryption can be computationally expensive. This makes mass messaging more expensive and harder to hide on a compromised machine.
* Phishing is much more difficult because the sender's identity is required, it is cryptographically verifiable, and each contact's keys are unique.
* Only contact requests may be sent to the user with their contact request key. Other types of messages encrypted with it are silently dropped by the client software. Contact requests sent using encryption or signing keys other than those described in the process below MUST also be silently dropped. 
* Because the sender's verifiable identity is required, spamming people through the contact request mechanism is easily stopped.

The Contact Request process is unique in that the initial request is the only type of message that can be sent to a recipient without any prior contact having been made. As such, it is very strictly regulated. Users can -- and should -- be reported for sending spam via Contact Requests. Administrators are highly encouraged to suspend and/or terminate accounts which exhibit this behavior. 

*Contact Request: Stage 1 (Lookup)*

User #1 retrieves and validates User #2's keycard. The keycard for User #2 contains an encryption key used to encrypt the contact request. More information on the keycard validation process and keycards in general can be found in the https://gitlab.com/mensago/mensago-docs/-/blob/master/Identity%20Services%20Guide.adoc[Identity Services Guide].

*Contact Request: Stage 2 (Initiation)*

User #1 sends a request to User #2.

This request contains whatever personal contact information User #1 wishes to send. It is signed using the key that corresponds to the Contact Request Verification Key in User #1's keycard so that User #2 can verify that the request actually came from User #1. The request is encrypted with the Contact Request Encryption Key obtained from User #2's keycard so that no one except User #2 can read it. Once received, User #2 can determine if contact should be permitted.

When sending the Initiation message, User #1 is not required to provide any more personal information than that which is already available in their keycard. However, users are encouraged to share additional information to help the recipient validate who the sender is. Any contact information field found in this specification can be part the contact request payload. A sample payload is shown below.

[source,json]
----
{
    "Type" : "application/vnd.mensago.message",
    "Version" : "1.0.0",
    "Subtype" : "conreq.1",
    "From" : "3cb11ab3-5482-4154-8ca1-dfa1cc79371c/example.com",
    "To" : "662679bd-3611-4d5e-a570-52812bdcc6f3/example.net",
    "Date" : "2019-09-05T15:53:23Z",
    "ContactInfo" : {
        "Version" : "1.0",
        "EntityType" : "individual",
        "ID" : "653d2304-20ce-40ac-b096-14240b23b1ae",

        "FormattedName" : "Richard Brannan",
        "GivenName" : "Richard",
        "FamilyName" : "Brannan",
        "Gender" : "Male",
        "Prefix" : "Mr.",
        "Suffix" : [ "Ph.D" ],
        "Label" : "Home",
        "UserID" : "cavs4life",
        "Workspace" : "e3e98b45-b226-4926-b4ea-69ab16dd035e",
        "Domain" : "example.com",
        "Keys" : {
            "Encryption": "CURVE25519:Umbw0Y<^cf1DN|>X38HCZO@Je(zSe6crC6X_C_0F",
            "Verification": "ED25519:6|HBWrxMY6-?r&Sm)_^PLPerpqOj#b&x#N_#C3}p"
        }
    },
    "Body" : "Richard Brannan is asking permission to contact you."
}
----

The contact information payload has 2 fields that are required for Contact Requests: two values in the `Keys` field, labeled `Encryption` and `Verification`. These fields contain public keys which are unique to the contact relationship are are used by the recipient to send messages to the sender, hereafter referred to as _relationship keys_. These keys enable greater security in that they can be rotated on a different schedule than the keys in the sender's keycard.

Unique to this specific contact request message are restrictions placed on the contents of the `Body` field. This field is intended to contain a short custom message from the sender to the receiver and MUST contain ONLY plain, unformatted text. Because Contact Request messages can be sent to anyone without prior consent, the `Body` field MUST be strictly controlled to prevent abuse.

- This field MUST NOT contain links, URLs, or encoded data of any kind.
- Clients MUST prevent the user from adding restricted content to this field when sending contact requests
- Clients MUST strip any restricted content from received contact requests.
- Clients MAY replace the restricted content with placeholder text indicating that something was removed, like "{restricted content removed}".

These requirements are to severely reduce, if not prevent, phishing attacks and spam. It is also _highly_ recommended that graphical clients use a multiline text field which has no capabilities to render HTML to provide an extra layer of protection.

*It cannot be stressed enough how critical control over the content of the `Body` field is to users' online safety*. Bad actors _will_ try to use it as an opportunity because it is the only unsolicited content on the Mensago platform that is allowed to be sent by an unverified sender. Implementors *MUST NOT* create extensions or customizations to this aspect of the protocol and MUST take all possible steps to ensure the safety of their users in this matter.

*Contact Request: Stage 3 (Response)*

When User #2 receives the Initiation message, they may drop the request and optionally block future requests. If User #2 approves the request, the approval message is sent with their contact information along with the requisite encryption and signature verification keys needed when contacting the user. This response message uses the subtype `conreq.2`. The response may contain a `Body` field, but should be ignored if present.

=== Contact Information Updates

One of the digital communication problems Mensago aims to solve is that of outdated contact information. Should a person change their mobile phone number, for example, their contacts can be quietly notified of the change.

Contact Information Update messages are sent for this very purpose. The message sent uses the subtype `conup`, but otherwise its fields and structure are exactly the same as an Initiation message. Also to be noted is that the update message is encrypted with the relationship key supplied by that entity during the Contact Request process, not the entity's contact request key.
